;;; Configuration for anything font related.  In particular, I want to specify
;;; a multiple fonts to be used in different instances (e.g., programming or
;;; text editing).

;; Set default font to be used.  This isn't super necessary because of further
;; configuration below, but in certain instances is needed for a good font to
;; be used.
(add-to-list 'default-frame-alist '(font . "Liberation Sans 11"))

;; Specify fonts to use
(setq my/variable-font "Liberation Sans"
      my/fixed-font "Fira Code")

(add-hook 'text-mode-hook 'my-buffer-face-mode-variable)
(add-hook 'prog-mode-hook 'my-buffer-face-mode-fixed)
