(global-unset-key (kbd "C-z"))
(global-set-key (kbd "C-z u") 'unfill-paragraph)
(global-set-key (kbd "C-z e") 'elfeed)
(global-set-key (kbd "C-z b") 'blamer-show-commit-info)
(global-set-key (kbd "C-z g") 'elgrep)
(global-set-key (kbd "C-z d b") 'nuke-all-buffers)

(global-set-key (kbd "C-z l D") 'lsp-find-declaration)
(global-set-key (kbd "C-z l d") 'lsp-find-definition)
(global-set-key (kbd "C-z l f") 'lsp-find-references)
(global-set-key (kbd "C-z l r") 'lsp-rename)

(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
