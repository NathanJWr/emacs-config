;;; Utility functions that I find useful

;; Package to kee ~* and #*#, etc. out of the directory I'm working in.
(use-package no-littering)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(defun nuke-all-buffers ()
  (interactive)
  (mapcar 'kill-buffer (buffer-list))
  (delete-other-windows))

(defun unfill-region ()
  "Converts a multi-line region into a single line."
  (interactive)
  (let ((fill-column (point-max)))
    (fill-region (region-beginning) (region-end) nil)))

;; It is the opposite of fill-paragraph    
(defun unfill-paragraph ()
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))

;; Use variable width font faces in current buffer
(defun my-buffer-face-mode-variable ()
  "Set font to a variable width (proportional) fonts in current buffer"
  (interactive)
  (setq buffer-face-mode-face
	`(:family ,my/variable-font :height 120 :width semi-condensed))
  (buffer-face-mode))

;; Use monospaced font faces in current buffer
(defun my-buffer-face-mode-fixed ()
  "Sets a fixed width (monospace) font in current buffer"
  (interactive)
  (setq buffer-face-mode-face
	`(:family ,my/fixed-font :height 120))
  (buffer-face-mode))


