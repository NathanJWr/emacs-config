;;; Configuration dedicated to making Emacs look nice

;; Nicely whoe where the cursor is when big screen changes/navigation happen.
(use-package beacon
  :config
  (beacon-mode 1))

;; Make writing in text files feel more like a word-processor.
(use-package olivetti
  :hook ((text-mode . olivetti-mode)))

;; A nicer looking modeline.  You need to install extra fonts to use this.
(use-package doom-modeline
  :config
  (doom-modeline-mode))

;; Set our theme!
(use-package doom-themes
  :config
  (load-theme 'adwaita t))
