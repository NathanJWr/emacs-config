(use-package magit)

;; Easily get the blame of whatever line you're on
(use-package blamer
  :defer 20
  :custom
  (blamer-idle-time 0.3)
  (blamer-min-offset 70))
