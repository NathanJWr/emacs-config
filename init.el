(require 'package)

;; Packages I want (or need, depending on your perspective) are only accessible
;; here on melpa.
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(require 'use-package)
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Load a lot of different configuration files that are relatively (🤷) well named.
;; These files should all be located in the ~/.emcacs.d directory
(dolist
    (path
     '("nathan-font"
       "nathan-util"
       "nathan-style"
       "nathan-org"
       "nathan-menus"
       "nathan-git"
       "nathan-programs"
       "nathan-programming"
       "nathan-keybinds"))
  (load (locate-user-emacs-file path)))
