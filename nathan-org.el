(use-package org
  :config
  (setq org-adapt-indentation nil)
  (electric-indent-mode -1))

(use-package flyspell
  :hook ((text-mode . flyspell-mode)))

;; This is a function because I don't want to have to have org-roam
;; trying to access a directory at startup.  If I'm working on a
;; computer that doesn't have my roam directory then it would just
;; complain.
(defun notes-init ()
  "Start org-roam with by specifying a directory."
  (interactive)
  (setq my/notes-dir (read-file-name "Notes directory: "))
  (use-package org-roam
    :custom
    (org-roam-directory (file-truename my/notes-dir))
    :bind (("C-c n l" . org-roam-buffer-toggle)
	   ("C-c n f" . org-roam-node-find)
	   ("C-c n g" . org-roam-graph)
	   ("C-c n i" . org-roam-node-insert)
	   ("C-c n c" . org-roam-capture)
	   ("C-c n t" . org-roam-tag-add)
                          ;; Dailies
	   ("C-c n j" . org-roam-dailies-capture-today))
    :config
    ;; If you're using a vertical completion framework, you might want a more informative completion interface
    (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
    (org-roam-db-autosync-mode)))

